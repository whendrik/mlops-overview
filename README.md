
## General *-Ops tools

https://jenkins.io/


## Data-Science Lifecycle

https://towardsdatascience.com/data-science-life-cycle-101-for-dummies-like-me-e66b47ad8d8f

## Frameworks

### Overview

https://github.com/1duo/awesome-ai-infrastructures

### Other Tools / *-Flows

https://www.kubeflow.org/

https://metaflow.org/

https://mlflow.org/

https://valohai.com/

https://dvc.org/

https://github.com/gojek/feast

https://drivendata.github.io/cookiecutter-data-science/

https://dotscience.com/manifesto/

## Youtube

https://www.youtube.com/watch?v=XV5VGddmP24